package com.bhushan;

import java.util.Scanner;

public class UpperCaseConcept {

	public static void main(String[] args) {

		String name;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Name");
		name = scanner.nextLine();

		String upperCaseName = name.toUpperCase();
		System.out.println(upperCaseName);

	}

}
