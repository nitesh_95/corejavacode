package com.bhushan;

import java.util.Scanner;

public class ArmStrongNumber {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		int number, reversedNumber = 0, remainder, temp;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		number = scanner.nextInt();

		temp = number;

		while (number != 0) {
			remainder = number % 10;
			number = number / 10;
			reversedNumber = reversedNumber + (remainder * remainder * remainder);

		}
		if (temp == reversedNumber) {
			System.out.println("Its Armstrong Number");
		} else {
			System.out.println("Not Armstrong");

		}

	}

}
