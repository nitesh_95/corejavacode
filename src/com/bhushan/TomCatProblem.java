package com.bhushan;

import java.util.Scanner;

public class TomCatProblem {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int no = scanner.nextInt();

		if (no % 3 == 0 && no % 5 == 0) {
			System.out.println("Tom" + "" + "Cat");
		} else if (no % 5 == 0) {
			System.out.println("cat");
		} else if (no % 3 == 0) {
			System.out.println("Tom");

		} else {
			System.out.println("Nothing from Above");
		}
	}
}
