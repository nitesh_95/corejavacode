package com.bhushan;

public class Reverse {

	public static void main(String[] args) {
		
		String name = "Nitesh";

		StringBuffer stringBuffer = new StringBuffer(name);
		StringBuffer reverse = stringBuffer.reverse();
		System.out.println(reverse);
	}

}
